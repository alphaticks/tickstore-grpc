/**
 * @fileoverview gRPC-Web generated client stub for tickstore_grpc
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.tickstore_grpc = require('./protos_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.tickstore_grpc.StoreClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.tickstore_grpc.StorePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.tickstore_grpc.StoreQueryRequest,
 *   !proto.tickstore_grpc.StoreQueryBatch>}
 */
const methodDescriptor_Store_Query = new grpc.web.MethodDescriptor(
  '/tickstore_grpc.Store/Query',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.tickstore_grpc.StoreQueryRequest,
  proto.tickstore_grpc.StoreQueryBatch,
  /**
   * @param {!proto.tickstore_grpc.StoreQueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.tickstore_grpc.StoreQueryBatch.deserializeBinary
);


/**
 * @param {!proto.tickstore_grpc.StoreQueryRequest} request The request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.tickstore_grpc.StoreQueryBatch>}
 *     The XHR Node Readable Stream
 */
proto.tickstore_grpc.StoreClient.prototype.query =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/tickstore_grpc.Store/Query',
      request,
      metadata || {},
      methodDescriptor_Store_Query);
};


/**
 * @param {!proto.tickstore_grpc.StoreQueryRequest} request The request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.tickstore_grpc.StoreQueryBatch>}
 *     The XHR Node Readable Stream
 */
proto.tickstore_grpc.StorePromiseClient.prototype.query =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/tickstore_grpc.Store/Query',
      request,
      metadata || {},
      methodDescriptor_Store_Query);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.tickstore_grpc.RegisterMeasurementRequest,
 *   !proto.tickstore_grpc.RegisterMeasurementResponse>}
 */
const methodDescriptor_Store_RegisterMeasurement = new grpc.web.MethodDescriptor(
  '/tickstore_grpc.Store/RegisterMeasurement',
  grpc.web.MethodType.UNARY,
  proto.tickstore_grpc.RegisterMeasurementRequest,
  proto.tickstore_grpc.RegisterMeasurementResponse,
  /**
   * @param {!proto.tickstore_grpc.RegisterMeasurementRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.tickstore_grpc.RegisterMeasurementResponse.deserializeBinary
);


/**
 * @param {!proto.tickstore_grpc.RegisterMeasurementRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.tickstore_grpc.RegisterMeasurementResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.tickstore_grpc.RegisterMeasurementResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.tickstore_grpc.StoreClient.prototype.registerMeasurement =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/tickstore_grpc.Store/RegisterMeasurement',
      request,
      metadata || {},
      methodDescriptor_Store_RegisterMeasurement,
      callback);
};


/**
 * @param {!proto.tickstore_grpc.RegisterMeasurementRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.tickstore_grpc.RegisterMeasurementResponse>}
 *     Promise that resolves to the response
 */
proto.tickstore_grpc.StorePromiseClient.prototype.registerMeasurement =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/tickstore_grpc.Store/RegisterMeasurement',
      request,
      metadata || {},
      methodDescriptor_Store_RegisterMeasurement);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.tickstore_grpc.GetMeasurementsRequest,
 *   !proto.tickstore_grpc.GetMeasurementsResponse>}
 */
const methodDescriptor_Store_GetMeasurements = new grpc.web.MethodDescriptor(
  '/tickstore_grpc.Store/GetMeasurements',
  grpc.web.MethodType.UNARY,
  proto.tickstore_grpc.GetMeasurementsRequest,
  proto.tickstore_grpc.GetMeasurementsResponse,
  /**
   * @param {!proto.tickstore_grpc.GetMeasurementsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.tickstore_grpc.GetMeasurementsResponse.deserializeBinary
);


/**
 * @param {!proto.tickstore_grpc.GetMeasurementsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.tickstore_grpc.GetMeasurementsResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.tickstore_grpc.GetMeasurementsResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.tickstore_grpc.StoreClient.prototype.getMeasurements =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/tickstore_grpc.Store/GetMeasurements',
      request,
      metadata || {},
      methodDescriptor_Store_GetMeasurements,
      callback);
};


/**
 * @param {!proto.tickstore_grpc.GetMeasurementsRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.tickstore_grpc.GetMeasurementsResponse>}
 *     Promise that resolves to the response
 */
proto.tickstore_grpc.StorePromiseClient.prototype.getMeasurements =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/tickstore_grpc.Store/GetMeasurements',
      request,
      metadata || {},
      methodDescriptor_Store_GetMeasurements);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.tickstore_grpc.DeleteMeasurementRequest,
 *   !proto.tickstore_grpc.DeleteMeasurementResponse>}
 */
const methodDescriptor_Store_DeleteMeasurement = new grpc.web.MethodDescriptor(
  '/tickstore_grpc.Store/DeleteMeasurement',
  grpc.web.MethodType.UNARY,
  proto.tickstore_grpc.DeleteMeasurementRequest,
  proto.tickstore_grpc.DeleteMeasurementResponse,
  /**
   * @param {!proto.tickstore_grpc.DeleteMeasurementRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.tickstore_grpc.DeleteMeasurementResponse.deserializeBinary
);


/**
 * @param {!proto.tickstore_grpc.DeleteMeasurementRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.tickstore_grpc.DeleteMeasurementResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.tickstore_grpc.DeleteMeasurementResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.tickstore_grpc.StoreClient.prototype.deleteMeasurement =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/tickstore_grpc.Store/DeleteMeasurement',
      request,
      metadata || {},
      methodDescriptor_Store_DeleteMeasurement,
      callback);
};


/**
 * @param {!proto.tickstore_grpc.DeleteMeasurementRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.tickstore_grpc.DeleteMeasurementResponse>}
 *     Promise that resolves to the response
 */
proto.tickstore_grpc.StorePromiseClient.prototype.deleteMeasurement =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/tickstore_grpc.Store/DeleteMeasurement',
      request,
      metadata || {},
      methodDescriptor_Store_DeleteMeasurement);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.tickstore_grpc.GetLastEventTimeRequest,
 *   !proto.tickstore_grpc.GetLastEventTimeResponse>}
 */
const methodDescriptor_Store_GetLastEventTime = new grpc.web.MethodDescriptor(
  '/tickstore_grpc.Store/GetLastEventTime',
  grpc.web.MethodType.UNARY,
  proto.tickstore_grpc.GetLastEventTimeRequest,
  proto.tickstore_grpc.GetLastEventTimeResponse,
  /**
   * @param {!proto.tickstore_grpc.GetLastEventTimeRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.tickstore_grpc.GetLastEventTimeResponse.deserializeBinary
);


/**
 * @param {!proto.tickstore_grpc.GetLastEventTimeRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.tickstore_grpc.GetLastEventTimeResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.tickstore_grpc.GetLastEventTimeResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.tickstore_grpc.StoreClient.prototype.getLastEventTime =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/tickstore_grpc.Store/GetLastEventTime',
      request,
      metadata || {},
      methodDescriptor_Store_GetLastEventTime,
      callback);
};


/**
 * @param {!proto.tickstore_grpc.GetLastEventTimeRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.tickstore_grpc.GetLastEventTimeResponse>}
 *     Promise that resolves to the response
 */
proto.tickstore_grpc.StorePromiseClient.prototype.getLastEventTime =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/tickstore_grpc.Store/GetLastEventTime',
      request,
      metadata || {},
      methodDescriptor_Store_GetLastEventTime);
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.tickstore_grpc.StorageClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.tickstore_grpc.StoragePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.tickstore_grpc.StorageQueryRequest,
 *   !proto.tickstore_grpc.StorageQueryResponse>}
 */
const methodDescriptor_Storage_Query = new grpc.web.MethodDescriptor(
  '/tickstore_grpc.Storage/Query',
  grpc.web.MethodType.UNARY,
  proto.tickstore_grpc.StorageQueryRequest,
  proto.tickstore_grpc.StorageQueryResponse,
  /**
   * @param {!proto.tickstore_grpc.StorageQueryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.tickstore_grpc.StorageQueryResponse.deserializeBinary
);


/**
 * @param {!proto.tickstore_grpc.StorageQueryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.tickstore_grpc.StorageQueryResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.tickstore_grpc.StorageQueryResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.tickstore_grpc.StorageClient.prototype.query =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/tickstore_grpc.Storage/Query',
      request,
      metadata || {},
      methodDescriptor_Storage_Query,
      callback);
};


/**
 * @param {!proto.tickstore_grpc.StorageQueryRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.tickstore_grpc.StorageQueryResponse>}
 *     Promise that resolves to the response
 */
proto.tickstore_grpc.StoragePromiseClient.prototype.query =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/tickstore_grpc.Storage/Query',
      request,
      metadata || {},
      methodDescriptor_Storage_Query);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.tickstore_grpc.OpenRequest,
 *   !proto.tickstore_grpc.StorageHandleBatch>}
 */
const methodDescriptor_Storage_Open = new grpc.web.MethodDescriptor(
  '/tickstore_grpc.Storage/Open',
  grpc.web.MethodType.SERVER_STREAMING,
  proto.tickstore_grpc.OpenRequest,
  proto.tickstore_grpc.StorageHandleBatch,
  /**
   * @param {!proto.tickstore_grpc.OpenRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.tickstore_grpc.StorageHandleBatch.deserializeBinary
);


/**
 * @param {!proto.tickstore_grpc.OpenRequest} request The request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.tickstore_grpc.StorageHandleBatch>}
 *     The XHR Node Readable Stream
 */
proto.tickstore_grpc.StorageClient.prototype.open =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/tickstore_grpc.Storage/Open',
      request,
      metadata || {},
      methodDescriptor_Storage_Open);
};


/**
 * @param {!proto.tickstore_grpc.OpenRequest} request The request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.tickstore_grpc.StorageHandleBatch>}
 *     The XHR Node Readable Stream
 */
proto.tickstore_grpc.StoragePromiseClient.prototype.open =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/tickstore_grpc.Storage/Open',
      request,
      metadata || {},
      methodDescriptor_Storage_Open);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.tickstore_grpc.GetMeasurementsRequest,
 *   !proto.tickstore_grpc.GetMeasurementsResponse>}
 */
const methodDescriptor_Storage_GetMeasurements = new grpc.web.MethodDescriptor(
  '/tickstore_grpc.Storage/GetMeasurements',
  grpc.web.MethodType.UNARY,
  proto.tickstore_grpc.GetMeasurementsRequest,
  proto.tickstore_grpc.GetMeasurementsResponse,
  /**
   * @param {!proto.tickstore_grpc.GetMeasurementsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.tickstore_grpc.GetMeasurementsResponse.deserializeBinary
);


/**
 * @param {!proto.tickstore_grpc.GetMeasurementsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.tickstore_grpc.GetMeasurementsResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.tickstore_grpc.GetMeasurementsResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.tickstore_grpc.StorageClient.prototype.getMeasurements =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/tickstore_grpc.Storage/GetMeasurements',
      request,
      metadata || {},
      methodDescriptor_Storage_GetMeasurements,
      callback);
};


/**
 * @param {!proto.tickstore_grpc.GetMeasurementsRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.tickstore_grpc.GetMeasurementsResponse>}
 *     Promise that resolves to the response
 */
proto.tickstore_grpc.StoragePromiseClient.prototype.getMeasurements =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/tickstore_grpc.Storage/GetMeasurements',
      request,
      metadata || {},
      methodDescriptor_Storage_GetMeasurements);
};


module.exports = proto.tickstore_grpc;

