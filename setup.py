from setuptools import setup

setup(
    name='tickstore-grpc',
    version='1.0',
    packages=[''],
    url='https://gitlab.com/alphaticks/tickstore-grpc',
    license='copyright',
    author='Laurent Meunier',
    description='gRPC code for tickstore client'
)
