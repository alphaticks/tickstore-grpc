module gitlab.com/alphaticks/tickstore-grpc

go 1.13

require (
	github.com/gogo/protobuf v1.3.2
	google.golang.org/grpc v1.35.0
)
