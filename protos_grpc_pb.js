// GENERATED CODE -- DO NOT EDIT!

'use strict';
var protos_pb = require('./protos_pb.js');

function serialize_tickstore_grpc_DeleteMeasurementRequest(arg) {
  if (!(arg instanceof protos_pb.DeleteMeasurementRequest)) {
    throw new Error('Expected argument of type tickstore_grpc.DeleteMeasurementRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_DeleteMeasurementRequest(buffer_arg) {
  return protos_pb.DeleteMeasurementRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_DeleteMeasurementResponse(arg) {
  if (!(arg instanceof protos_pb.DeleteMeasurementResponse)) {
    throw new Error('Expected argument of type tickstore_grpc.DeleteMeasurementResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_DeleteMeasurementResponse(buffer_arg) {
  return protos_pb.DeleteMeasurementResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_GetLastEventTimeRequest(arg) {
  if (!(arg instanceof protos_pb.GetLastEventTimeRequest)) {
    throw new Error('Expected argument of type tickstore_grpc.GetLastEventTimeRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_GetLastEventTimeRequest(buffer_arg) {
  return protos_pb.GetLastEventTimeRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_GetLastEventTimeResponse(arg) {
  if (!(arg instanceof protos_pb.GetLastEventTimeResponse)) {
    throw new Error('Expected argument of type tickstore_grpc.GetLastEventTimeResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_GetLastEventTimeResponse(buffer_arg) {
  return protos_pb.GetLastEventTimeResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_GetMeasurementsRequest(arg) {
  if (!(arg instanceof protos_pb.GetMeasurementsRequest)) {
    throw new Error('Expected argument of type tickstore_grpc.GetMeasurementsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_GetMeasurementsRequest(buffer_arg) {
  return protos_pb.GetMeasurementsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_GetMeasurementsResponse(arg) {
  if (!(arg instanceof protos_pb.GetMeasurementsResponse)) {
    throw new Error('Expected argument of type tickstore_grpc.GetMeasurementsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_GetMeasurementsResponse(buffer_arg) {
  return protos_pb.GetMeasurementsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_OpenRequest(arg) {
  if (!(arg instanceof protos_pb.OpenRequest)) {
    throw new Error('Expected argument of type tickstore_grpc.OpenRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_OpenRequest(buffer_arg) {
  return protos_pb.OpenRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_RegisterMeasurementRequest(arg) {
  if (!(arg instanceof protos_pb.RegisterMeasurementRequest)) {
    throw new Error('Expected argument of type tickstore_grpc.RegisterMeasurementRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_RegisterMeasurementRequest(buffer_arg) {
  return protos_pb.RegisterMeasurementRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_RegisterMeasurementResponse(arg) {
  if (!(arg instanceof protos_pb.RegisterMeasurementResponse)) {
    throw new Error('Expected argument of type tickstore_grpc.RegisterMeasurementResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_RegisterMeasurementResponse(buffer_arg) {
  return protos_pb.RegisterMeasurementResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_StorageHandleBatch(arg) {
  if (!(arg instanceof protos_pb.StorageHandleBatch)) {
    throw new Error('Expected argument of type tickstore_grpc.StorageHandleBatch');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_StorageHandleBatch(buffer_arg) {
  return protos_pb.StorageHandleBatch.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_StorageQueryRequest(arg) {
  if (!(arg instanceof protos_pb.StorageQueryRequest)) {
    throw new Error('Expected argument of type tickstore_grpc.StorageQueryRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_StorageQueryRequest(buffer_arg) {
  return protos_pb.StorageQueryRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_StorageQueryResponse(arg) {
  if (!(arg instanceof protos_pb.StorageQueryResponse)) {
    throw new Error('Expected argument of type tickstore_grpc.StorageQueryResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_StorageQueryResponse(buffer_arg) {
  return protos_pb.StorageQueryResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_StoreQueryBatch(arg) {
  if (!(arg instanceof protos_pb.StoreQueryBatch)) {
    throw new Error('Expected argument of type tickstore_grpc.StoreQueryBatch');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_StoreQueryBatch(buffer_arg) {
  return protos_pb.StoreQueryBatch.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_StoreQueryRequest(arg) {
  if (!(arg instanceof protos_pb.StoreQueryRequest)) {
    throw new Error('Expected argument of type tickstore_grpc.StoreQueryRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_StoreQueryRequest(buffer_arg) {
  return protos_pb.StoreQueryRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_TickEventBatch(arg) {
  if (!(arg instanceof protos_pb.TickEventBatch)) {
    throw new Error('Expected argument of type tickstore_grpc.TickEventBatch');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_TickEventBatch(buffer_arg) {
  return protos_pb.TickEventBatch.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tickstore_grpc_WriteAck(arg) {
  if (!(arg instanceof protos_pb.WriteAck)) {
    throw new Error('Expected argument of type tickstore_grpc.WriteAck');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tickstore_grpc_WriteAck(buffer_arg) {
  return protos_pb.WriteAck.deserializeBinary(new Uint8Array(buffer_arg));
}


var StoreService = exports['tickstore_grpc.Store'] = {
  write: {
    path: '/tickstore_grpc.Store/Write',
    requestStream: true,
    responseStream: true,
    requestType: protos_pb.TickEventBatch,
    responseType: protos_pb.WriteAck,
    requestSerialize: serialize_tickstore_grpc_TickEventBatch,
    requestDeserialize: deserialize_tickstore_grpc_TickEventBatch,
    responseSerialize: serialize_tickstore_grpc_WriteAck,
    responseDeserialize: deserialize_tickstore_grpc_WriteAck,
  },
  query: {
    path: '/tickstore_grpc.Store/Query',
    requestStream: false,
    responseStream: true,
    requestType: protos_pb.StoreQueryRequest,
    responseType: protos_pb.StoreQueryBatch,
    requestSerialize: serialize_tickstore_grpc_StoreQueryRequest,
    requestDeserialize: deserialize_tickstore_grpc_StoreQueryRequest,
    responseSerialize: serialize_tickstore_grpc_StoreQueryBatch,
    responseDeserialize: deserialize_tickstore_grpc_StoreQueryBatch,
  },
  registerMeasurement: {
    path: '/tickstore_grpc.Store/RegisterMeasurement',
    requestStream: false,
    responseStream: false,
    requestType: protos_pb.RegisterMeasurementRequest,
    responseType: protos_pb.RegisterMeasurementResponse,
    requestSerialize: serialize_tickstore_grpc_RegisterMeasurementRequest,
    requestDeserialize: deserialize_tickstore_grpc_RegisterMeasurementRequest,
    responseSerialize: serialize_tickstore_grpc_RegisterMeasurementResponse,
    responseDeserialize: deserialize_tickstore_grpc_RegisterMeasurementResponse,
  },
  getMeasurements: {
    path: '/tickstore_grpc.Store/GetMeasurements',
    requestStream: false,
    responseStream: false,
    requestType: protos_pb.GetMeasurementsRequest,
    responseType: protos_pb.GetMeasurementsResponse,
    requestSerialize: serialize_tickstore_grpc_GetMeasurementsRequest,
    requestDeserialize: deserialize_tickstore_grpc_GetMeasurementsRequest,
    responseSerialize: serialize_tickstore_grpc_GetMeasurementsResponse,
    responseDeserialize: deserialize_tickstore_grpc_GetMeasurementsResponse,
  },
  deleteMeasurement: {
    path: '/tickstore_grpc.Store/DeleteMeasurement',
    requestStream: false,
    responseStream: false,
    requestType: protos_pb.DeleteMeasurementRequest,
    responseType: protos_pb.DeleteMeasurementResponse,
    requestSerialize: serialize_tickstore_grpc_DeleteMeasurementRequest,
    requestDeserialize: deserialize_tickstore_grpc_DeleteMeasurementRequest,
    responseSerialize: serialize_tickstore_grpc_DeleteMeasurementResponse,
    responseDeserialize: deserialize_tickstore_grpc_DeleteMeasurementResponse,
  },
  getLastEventTime: {
    path: '/tickstore_grpc.Store/GetLastEventTime',
    requestStream: false,
    responseStream: false,
    requestType: protos_pb.GetLastEventTimeRequest,
    responseType: protos_pb.GetLastEventTimeResponse,
    requestSerialize: serialize_tickstore_grpc_GetLastEventTimeRequest,
    requestDeserialize: deserialize_tickstore_grpc_GetLastEventTimeRequest,
    responseSerialize: serialize_tickstore_grpc_GetLastEventTimeResponse,
    responseDeserialize: deserialize_tickstore_grpc_GetLastEventTimeResponse,
  },
};

var StorageService = exports['tickstore_grpc.Storage'] = {
  query: {
    path: '/tickstore_grpc.Storage/Query',
    requestStream: false,
    responseStream: false,
    requestType: protos_pb.StorageQueryRequest,
    responseType: protos_pb.StorageQueryResponse,
    requestSerialize: serialize_tickstore_grpc_StorageQueryRequest,
    requestDeserialize: deserialize_tickstore_grpc_StorageQueryRequest,
    responseSerialize: serialize_tickstore_grpc_StorageQueryResponse,
    responseDeserialize: deserialize_tickstore_grpc_StorageQueryResponse,
  },
  open: {
    path: '/tickstore_grpc.Storage/Open',
    requestStream: false,
    responseStream: true,
    requestType: protos_pb.OpenRequest,
    responseType: protos_pb.StorageHandleBatch,
    requestSerialize: serialize_tickstore_grpc_OpenRequest,
    requestDeserialize: deserialize_tickstore_grpc_OpenRequest,
    responseSerialize: serialize_tickstore_grpc_StorageHandleBatch,
    responseDeserialize: deserialize_tickstore_grpc_StorageHandleBatch,
  },
  getMeasurements: {
    path: '/tickstore_grpc.Storage/GetMeasurements',
    requestStream: false,
    responseStream: false,
    requestType: protos_pb.GetMeasurementsRequest,
    responseType: protos_pb.GetMeasurementsResponse,
    requestSerialize: serialize_tickstore_grpc_GetMeasurementsRequest,
    requestDeserialize: deserialize_tickstore_grpc_GetMeasurementsRequest,
    responseSerialize: serialize_tickstore_grpc_GetMeasurementsResponse,
    responseDeserialize: deserialize_tickstore_grpc_GetMeasurementsResponse,
  },
};

